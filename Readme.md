# SafeTrace

## Overview

SafeTrace is written in Typescript and presently powered by Expo.js

SafeTrace allows users to capture their geo-location data and save it securely to their Personal Data Account on the Dataswift platform.

## Development

Getting up and running:

1. Install the expo-cli: `npm install expo-cli --global`
2. Install dependencies: `yarn install`
3. Run development environment: `yarn start`

### Testing

Presently we have unit test coverage using the jest test runner and testing-library/native-testing-library

To run the unit tests: `yarn test` or `yarn test:watch`

---

## Additional

### Accessing latest build via expo (Android only\*)

-   Download Expo from the Google Play Store: https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en_GB
-   Visit the SafeTrace expo URL: https://exp.host/@ryanhyslop/safetrace
-   Select 'Scan QR Code' in expo app and scan the QR code on the Expo webpage.
-   Dismiss the developer hint and close dev tools modal that appears using the close (x) icon in the top right of the popover.
-   And there we go 👍

*   iOS currently requires a custom expo client build signed with developer credentials for background location tracking.

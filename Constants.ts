export const TOKEN_STORAGE_KEY = 'data-swift-auth-token';
export const TASK_BACKGROUND_FETCH_NAME = '@safetrace:BACKGROUND_FETCH';
export const TASK_BACKGROUND_LOCATION_NAME = '@safetrace:BACKGROUND_LOCATION';

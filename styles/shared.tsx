import { StyleSheet } from 'react-native';

const sharedStyles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: 'white',
    },
    container: {
        padding: 20,
    },
});

export default sharedStyles;
